package org.example;

import com.beanit.asn1bean.ber.ReverseByteArrayOutputStream;
import com.beanit.asn1bean.ber.types.BerObjectIdentifier;
import com.beanit.asn1bean.ber.types.BerType;
import de.ets.cv.certificates.CvCertificate;
import de.ets.cv.certificates.PublicKeyParameters;
import de.ets.ka.common.SignatureAlgorithmId;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.operator.DefaultAlgorithmNameFinder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.spec.ECParameterSpec;
import java.util.Collections;

public class Main {
    public static final BouncyCastleProvider BOUNCY_CASTLE_PROVIDER = new BouncyCastleProvider();

    private static byte[] getBytes(final Iterable<BerType> elements) throws IOException {
        final ReverseByteArrayOutputStream bos = new ReverseByteArrayOutputStream(1000, true);
        for (final BerType element : elements) {
            element.encode(bos);
        }
        return bos.getArray();
    }

    private static byte[] getBytes(final BerType element) throws IOException {
        return getBytes(Collections.singleton(element));
    }

    private static PublicKey getPublicKey(final PublicKeyParameters publicKeyParameters) {
        final BerObjectIdentifier algorithmId = publicKeyParameters.getAlgorithmId();
        final byte[] extensionBytes = publicKeyParameters.getExtensionBytes();

        // get the VALUE part of the object P
        final byte[] encodedPublicKeyPoint = ((ASN1OctetString) ASN1TaggedObject.getInstance(extensionBytes).getBaseObject()).getOctets();

        final X9ECParameters ecParameters = ECNamedCurveTable.getByOID(new ASN1ObjectIdentifier(algorithmId.toString()));
        final ECCurve curve = ecParameters.getCurve();
        final ECPoint publicPoint = curve.decodePoint(encodedPublicKeyPoint);
        final ECPublicKeyParameters ecPubKeyParams = new ECPublicKeyParameters(publicPoint, new ECDomainParameters(ecParameters));
        final ECParameterSpec ecParameterSpec = EC5Util.convertToSpec(ecParameters);

        return new BCECPublicKey("EC", ecPubKeyParams, ecParameterSpec, BouncyCastleProvider.CONFIGURATION);
    }

    private static boolean verifySignature(
            final SignatureAlgorithmId signatureAlgorithmId,
            final PublicKeyParameters publicKeyParameters,
            final byte[] message,
            final byte[] signature
    ) throws SignatureException, InvalidKeyException, NoSuchAlgorithmException {
        final String signatureAlgorithmName = new DefaultAlgorithmNameFinder().getAlgorithmName(new ASN1ObjectIdentifier(signatureAlgorithmId.toString()));
        final Signature ecdsaVerifier = Signature.getInstance(signatureAlgorithmName, BOUNCY_CASTLE_PROVIDER);
        ecdsaVerifier.initVerify(getPublicKey(publicKeyParameters));
        ecdsaVerifier.update(message);
        return ecdsaVerifier.verify(signature);
    }

    public static void main(String[] args) {
        Security.addProvider(BOUNCY_CASTLE_PROVIDER);

        final CvCertificate cvCertificate = new CvCertificate();
        try (final InputStream inputStream = Main.class.getClassLoader().getResourceAsStream("testdata/cvc_root_01.der")) {
            cvCertificate.decode(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            boolean result = verifySignature(
                    cvCertificate.getCertificateBody().getSignatureAlgorithmId(),
                    // only works for the self-signed case
                    cvCertificate.getCertificateBody().getPublicKeyParameters(),
                    // signed parts are always with TAG and LENGTH
                    getBytes(cvCertificate.getCertificateBody()),
                    // get the VALUE part of the signature
                    cvCertificate.getSignature().value
            );

            if (result) {
                System.out.println("Signature verification succeeded!");
            } else {
                System.err.println("Signature verification failed!");
                System.exit(-1);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
